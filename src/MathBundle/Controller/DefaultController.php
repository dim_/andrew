<?php

namespace MathBundle\Controller;

use MathBundle\Entity\Flower;
use MathBundle\Entity\Answer;
use MathBundle\Entity\Symptom;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function addKnowledgeAction(Request $request){
        $answer_str = $this->flowerRepo()->getAnswerByRequest($request);
        $answer = $this->answerRepo()->findOneBy([
            'answer' => $answer_str
        ]);
        
        if(!$answer instanceof Answer){
            $answer = new Answer();
            $answer->setAnswer($answer_str);
            $answer->setFlower($this->flowerRepo()->find($request->get('flower')));
            $answer->setStatus(false);
            $this->plush($answer);
        }
        
        return $this->redirectToRoute('learn');
    }
    
    public function getAnswerAction(Request $request){
        $id = $this->flowerRepo()->getIdByRequest($request, $this->answerRepo());
        $answer = $this->flowerRepo()->find($id);

        if(!$answer instanceof Flower){
            $answer = null;
        }

        return $this->render('@Math/Default/startTest.html.twig', [
            'symptoms' => $this->symptomRepo()->getSymptoms(),
            'flowers' => null,
            'answer' => $answer
        ]);
    }
    
    public function startTestAction(){

        return $this->render('@Math/Default/startTest.html.twig', [
            'symptoms' => $this->symptomRepo()->getSymptoms(),
            'flowers' => null,
            'answer' => null
        ]);
    }
    
    public function autoStartAction(){
        
        return $this->render('@Math/Default/autoLearning.twig', [
            'countSymptoms' => 0,
            'messages' => []
        ]);
    }
    

    public function autoLearningAction(){
        $messages = [];
        $iterations = 0; 
        $flag = true;
        $count = 0;
        
        while ($flag){
            $flag = false;
            $iterations++;
            $count = 0;
            $answers = $this->answerRepo()->findBy([
                'status' => false
            ]);
            /** @var Symptom[] $symptoms */
            $symptoms = $this->symptomRepo()->getSymptoms();
            /** @var Flower[] $flowers_objs */
            $flowers_objs = $this->flowerRepo()->findAll();

            /** @var mixed $flowers */

            foreach ($answers as $answer){

                foreach ($flowers_objs as $flower){
                    $flowers[$flower->getId()] = 0;
                }

                $checked_symptoms = [];

                $id = $max = -1;
                $str = str_split($answer->getAnswer());
                $i = -1;

                foreach ($str as $item){
                    $i++;

                    if($item == '1'){
                        $checked_symptoms[] = $symptoms[$i]->getId();
                        $flowers[$symptoms[$i]->getFlower()->getId()] += $symptoms[$i]->getWeightFactor();

                        if($flowers[$symptoms[$i]->getFlower()->getId()] > $max){
                            $max = $flowers[$symptoms[$i]->getFlower()->getId()];
                            $id = $symptoms[$i]->getFlower()->getId();
                        }
                    }
                }

                /** @var Flower $temp_flow */
                $temp_flow = $answer->getFlower();
                $real_id = $temp_flow->getId();

                if($real_id > 0 && $id > 0 && $real_id != $id){
                    $this->learning($real_id,$id,$count,$checked_symptoms);
                    /** @var Flower $n_flower */
                    $n_flower = $answer->getFlower();
                    $flag = true;
                    $messages[] = 'System: now I try to learn the symptoms of '.$n_flower->getName().'.';
                }else{
                    $answer->setStatus(true);
                    $this->plush($answer);
                    /** @var Flower $n_flower */
                    $n_flower = $answer->getFlower();
                    $messages[] = 'System: now I know about symptoms of '.$n_flower->getName().'.';
                }
            }
            
            if($iterations == 10){
                break;
            }
            
        }
        
        


        return $this->render('@Math/Default/autoLearning.twig', [
            'countSymptoms' => $count,
            'messages' => $messages
        ]);
    }

    public function learning(&$real_id, &$id, &$count, &$checked_symptoms){
        $count++;
        $real_flow = $this->flowerRepo()->find($real_id);
        $temp_flow = $this->flowerRepo()->find($id);
        /** @var Symptom[] $real_symps */
        $real_symps = $real_flow->getSymptoms();
        /** @var Symptom[] $temp_symps */
        $temp_symps = $temp_flow->getSymptoms();
        $temp_plus = $real_plus = 0;
        $real_ids = $temp_ids = [];

        $this->incDec($checked_symptoms,$real_symps,$temp_symps,$real_plus,$temp_plus, $real_ids, $temp_ids);

        $real_znam = (count($real_symps) - $real_plus);

        if($real_znam < 1){
            $real_znam = 1;
        }

        $real_minus = round($real_plus / $real_znam,2);
        $i = -1;
        $real_sum = 0;
        $real_finish_id = null;

        foreach ($real_symps as $real_symp){
            $i++;

            if($real_symp->getId() != $real_ids[$i]){
                $real_finish_id = $real_symp->getId();
                $real_symp->setWeightFactor($real_symp->getWeightFactor() - 1);
                $this->plush($real_symp);
            }
            $real_sum += $real_symp->getWeightFactor();
        }

        if($real_sum != 100){
            $real_symp = $this->symptomRepo()->find($real_finish_id);
            $real_symp->setWeightFactor($real_symp->getWeightFactor() + (100 - $real_sum));
            $this->plush($real_symp);
        }

        $temp_znam = (count($temp_symps) - $temp_plus);

        if($temp_znam < 1){
            $temp_znam = 1;
        }

        $temp_minus = round($temp_plus / $temp_znam,2);
        $i = -1;
        $temp_sum = 0;
        $temp_finish_id = null;

        foreach ($temp_symps as $temp_symp){
            $i++;

            if($temp_symp->getId() != $temp_ids[$i]){
                $temp_finish_id = $temp_symp->getId();
                $temp_symp->setWeightFactor($temp_symp->getWeightFactor() + 1);
                $this->plush($temp_symp);
            }
            $temp_sum += $temp_symp->getWeightFactor();
        }

        if($temp_sum != 100){
            $temp_symp = $this->symptomRepo()->find($temp_finish_id);
            $temp_symp->setWeightFactor($temp_symp->getWeightFactor() + (100 - $temp_sum));
            $this->plush($temp_symp);
        }
    }

    public function incDec(&$checked_symptoms, &$real_symps, &$temp_symps, &$real_plus, &$temp_plus, &$real_ids, &$temp_ids){
        /** @var Symptom[] $real_symps */
        /** @var Symptom[] $temp_symps */


        foreach ($checked_symptoms as $checked_symptom){

            foreach ($real_symps as $real_symp){

                if($checked_symptom == $real_symp->getId()){
                    $real_symp->setWeightFactor($real_symp->getWeightFactor()+1);
                    $this->plush($real_symp);
                    $real_plus++;
                    $real_ids[] = $real_symp->getId();
                }else{
                    $real_ids[] = -1;
                }
            }

            foreach ($temp_symps as $temp_symp){

                if($checked_symptom == $temp_symp->getId()){
                    $temp_symp->setWeightFactor($temp_symp->getWeightFactor()-1);
                    $this->plush($temp_symp);
                    $temp_plus++;
                    $temp_ids[] = $temp_symp->getId();
                }else{
                    $temp_ids[] = -1;
                }
            }
        }
    }
    

    public function startLearningAction(){

        return $this->render('@Math/Default/startTest.html.twig', [
            'symptoms' => $this->symptomRepo()->getSymptoms(),
            'flowers' => $this->flowerRepo()->findAll(),
            'answer' => null
        ]);
    }
    
    public function removeSymptomAction(Request $request){
        $symptom = $this->symptomRepo()->find($request->get('id'));
        
        if($symptom instanceof Symptom){
            $em = $this->em();
            $em->remove($symptom);
            $em->flush();
        }
        
        return $this->back($request);
    }

    public function removeFlowerAction(Request $request){
        $flower = $this->flowerRepo()->find($request->get('id'));

        if($flower instanceof Flower){
            $em = $this->em();
            $em->remove($flower);
            $em->flush();
        }

        return $this->back($request);
    }
    
    public function addSymptomAction(Request $request){
        
        $symptom = $this->checkSymptom($request);
        
        if(!$symptom instanceof Symptom) {
            $symptom = new Symptom();
            $symptom->setName($request->get('name'));
            $symptom->setFlower($this->flowerRepo()->find($request->get('id')));
        }
        
        $symptom->setWeightFactor($request->get('weight'));
        $this->plush($symptom);

        return $this->back($request);
    }
    
    public function editSymptomAction(Request $request, $id){
        $symptom = $this->symptomRepo()->find($id);
        
        if(!$symptom instanceof Symptom){
            
            return $this->back($request);
        }
        
        $symptom->setWeightFactor($request->get('weight'));
        $this->plush($symptom);
        
        return $this->back($request);
    }
    
    public function flowersAction(){

        return $this->render('@Math/Default/flowers.html.twig', [
            'flowers' => $this->flowerRepo()->findAll(),
            'allSymptoms' => $this->symptomRepo()->findAll(),
            'uniqSymptoms' => $this->getDoctrine()->getRepository('MathBundle:Symptom')->getUniqSymptoms()
        ]);
    }
    
    public function addFlowerFormAction(){

        return $this->render('@Math/Default/add_flower.html.twig');
    }
    
    public function addFlowerAction(Request $request){
        $flower = $this->checkFlower($request);

        if(!$flower instanceof Flower){
            $flower = new Flower();
        }

        $flower->setName($request->get('name'));
        $this->plush($flower);
        
        return $this->redirectToRoute('flowers');
    }

    public function indexAction(){

        return $this->render('@Math/Default/home.html.twig');
    }
    
    public function em(){
        
        return $this->getDoctrine()->getManager();
    }
    
    public function flowerRepo(){
        
        return $this->getDoctrine()->getRepository('MathBundle:Flower');
    }

    public function symptomRepo(){

        return $this->getDoctrine()->getRepository('MathBundle:Symptom');
    }

    public function answerRepo(){

        return $this->getDoctrine()->getRepository('MathBundle:Answer');
    }
    
    public function plush($entity){
        $em =$this->em();
        $em->persist($entity);
        $em->flush();
    }


    private function back(Request $request){
        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    public function checkSymptom(Request $request){
        $rp = $this->getDoctrine()->getRepository('MathBundle:Symptom');
        $flower = $this->flowerRepo()->find($request->get('id'));
        $symptom = $rp->findOneBy([
            'flower' => $flower,
            'name' => $request->get('name')
        ]);

        if($symptom instanceof Symptom){
            return $symptom;
        }
        

        return false;
    }

    public function checkFlower(Request $request){
        $flower = $this->flowerRepo()->findOneBy([
            'name' => $request->get('name')
        ]);

        if($flower instanceof Flower){

            return $flower;
        }

        return false;
    }
    
    
    public function newListAction(){
        
        return $this->render('@Math/Default/new_flowers.html.twig',['flowers' => $this->flowerRepo()->findAll()]);
    }
    
    public function newFlowerAction($id){
        $flower = $this->flowerRepo()->find($id);

        return $this->render('@Math/Default/new_flower.html.twig', [
            'flower' => $flower,
            'allSymptoms' => $this->symptomRepo()->findAll(),
            'uniqSymptoms' => $this->getDoctrine()->getRepository('MathBundle:Symptom')->getUniqSymptoms()
        ]);
    }
    
}

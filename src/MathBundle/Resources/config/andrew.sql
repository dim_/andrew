-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 08 2017 г., 16:54
-- Версия сервера: 5.7.19-0ubuntu0.16.04.1
-- Версия PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `andrew`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `flower_id` int(11) DEFAULT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `flower_id`, `answer`, `status`) VALUES
(2, 4, '000000111111111110001111111111111111110', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `flowers`
--

CREATE TABLE `flowers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `flowers`
--

INSERT INTO `flowers` (`id`, `name`) VALUES
(4, 'Орхидея'),
(5, 'Роза'),
(6, 'Гортензия'),
(7, 'Ландыш'),
(8, 'Бальзамин');

-- --------------------------------------------------------

--
-- Структура таблицы `symptoms`
--

CREATE TABLE `symptoms` (
  `id` int(11) NOT NULL,
  `flower_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weight_factor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `symptoms`
--

INSERT INTO `symptoms` (`id`, `flower_id`, `name`, `weight_factor`) VALUES
(5, 4, 'Домашние', 12),
(6, 4, 'Уличные', 12),
(7, 4, 'меньше 1м', 12),
(8, 4, 'дороже 100 грн', 16),
(9, 4, 'растет в Украине', 12),
(11, 4, 'не ядовитые', 12),
(12, 4, 'не в красной книге', 12),
(13, 5, 'Уличные', 12),
(15, 5, 'больше 1м', 12),
(16, 5, 'дешевле 100 грн', 16),
(17, 5, 'растет в Украине', 12),
(19, 5, 'не ядовитые', 12),
(20, 5, 'не в красной книге', 12),
(21, 5, 'Домашние', 12),
(22, 6, 'Уличные', 13),
(23, 6, 'меньше 1м', 13),
(24, 6, 'дороже 100 грн', 14),
(26, 6, 'не ядовитые', 12),
(27, 6, 'не в красной книге', 12),
(28, 6, 'Не домашние', 19),
(29, 7, 'Не домашние', 12),
(30, 7, 'Уличные', 12),
(31, 7, 'меньше 1м', 12),
(32, 7, 'дешевле 100 грн', 12),
(33, 7, 'растет в Украине', 12),
(35, 7, 'не ядовитые', 12),
(36, 7, 'В красной книге', 16),
(37, 8, 'Не домашние', 12),
(38, 8, 'Уличные', 12),
(39, 8, 'меньше 1м', 12),
(40, 8, 'дешевле 100 грн', 12),
(41, 8, 'растет в Украине', 12),
(43, 8, 'ядовитые', 16),
(44, 8, 'В красной книге', 12),
(46, 4, 'растет за рубежом', 12),
(47, 5, 'растет за рубежом', 12),
(48, 6, 'растет за рубежом', 17),
(49, 7, 'растет за рубежом', 12),
(50, 8, 'растет за рубежом', 12);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_50D0C6062C09D409` (`flower_id`);

--
-- Индексы таблицы `flowers`
--
ALTER TABLE `flowers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `symptoms`
--
ALTER TABLE `symptoms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CD3CAE132C09D409` (`flower_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `flowers`
--
ALTER TABLE `flowers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `symptoms`
--
ALTER TABLE `symptoms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `FK_50D0C6062C09D409` FOREIGN KEY (`flower_id`) REFERENCES `flowers` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `symptoms`
--
ALTER TABLE `symptoms`
  ADD CONSTRAINT `FK_CD3CAE132C09D409` FOREIGN KEY (`flower_id`) REFERENCES `flowers` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

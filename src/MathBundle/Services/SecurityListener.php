<?php
namespace MathBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class SecurityListener
{
    private $token;

    public function __construct(ContainerInterface $containerInterface, TokenStorage $tokenStorage)
    {
        $this->container = $containerInterface;
        $this->token = $tokenStorage->getToken();
    }
    
    public function getToken(){
        return $this->token;
    }

    public function onFinishRequestEvent(FinishRequestEvent $event)
    {
        //$this->container->get('doctrine')->getRepository('MathBundle:Invite')->getInvites($this->getToken());
    }

}
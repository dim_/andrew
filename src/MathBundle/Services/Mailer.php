<?php
namespace MathBundle\Services;

use Symfony\Component\HttpFoundation\Request;

class Mailer
{
    /**
     * The mailer
     *
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * The email address the mailer will send the emails from
     *
     * @var String
     */
    protected $emailFrom;


    /**
     * @param \Swift_Mailer|Request $mailer ;
     * @param $emailFrom
     */
    public function __construct(\Swift_Mailer $mailer, $emailFrom)
    {
        $this->mailer = $mailer;
        $this->emailFrom = $emailFrom;
    }

    /**
     * Compose email
     *
     * @param String $subject
     * @param String $recipientEmail
     * @param String $bodyHtml
     * @return \Swift_Message
     */
    public function composeEmail($subject, $recipientEmail, $bodyHtml)
    {
        /* @var $message \Swift_Message */
        $message = $this->mailer->createMessage();

        $message->setSubject($subject)
            ->setFrom($this->emailFrom)
            ->setTo($recipientEmail)
            ->setBody($bodyHtml, 'text/html');


        return $message;
    }


    /**
     * Send email
     *
     * @param \Swift_Message $message;
     */
    public function sendEmail(\Swift_Message $message)
    {
        if(!$this->mailer->getTransport()->isStarted()){
            $this->mailer->getTransport()->start();
        }

        $this->mailer->send($message);
        $this->mailer->getTransport()->stop();
    }



}
<?php

namespace MathBundle\Entity;

/**
 * Answer
 */
class Answer
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $answer;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \MathBundle\Entity\Flower
     */
    private $flower;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return Answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Answer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set flower
     *
     * @param \MathBundle\Entity\Flower $flower
     *
     * @return Answer
     */
    public function setFlower(\MathBundle\Entity\Flower $flower = null)
    {
        $this->flower = $flower;

        return $this;
    }

    /**
     * Get flower
     *
     * @return \MathBundle\Entity\Flower
     */
    public function getFlower()
    {
        return $this->flower;
    }
}

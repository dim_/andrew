<?php

namespace MathBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *  Flower
 */
class Flower
{
   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $symptoms;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->symptoms = new \Doctrine\Common\Collections\ArrayCollection();
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Flower
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add symptom
     *
     * @param \MathBundle\Entity\Symptom $symptom
     *
     * @return Flower
     */
    public function addSymptom(\MathBundle\Entity\Symptom $symptom)
    {
        $this->symptoms[] = $symptom;

        return $this;
    }

    /**
     * Remove symptom
     *
     * @param \MathBundle\Entity\Symptom $symptom
     */
    public function removeSymptom(\MathBundle\Entity\Symptom $symptom)
    {
        $this->symptoms->removeElement($symptom);
    }

    /**
     * Get symptoms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSymptoms()
    {
        return $this->symptoms;
    }

    /**
     * Add answer
     *
     * @param \MathBundle\Entity\Answer $answer
     *
     * @return Flower
     */
    public function addAnswer(\MathBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \MathBundle\Entity\Answer $answer
     */
    public function removeAnswer(\MathBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}

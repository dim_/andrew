<?php

namespace MathBundle\Entity;

/**
 * Symptom
 */
class Symptom
{
    
   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $weightFactor;

    /**
     * @var \MathBundle\Entity\Flower
     */
    private $flower;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Symptom
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set weightFactor
     *
     * @param integer $weightFactor
     *
     * @return Symptom
     */
    public function setWeightFactor($weightFactor)
    {
        $this->weightFactor = $weightFactor;

        return $this;
    }

    /**
     * Get weightFactor
     *
     * @return integer
     */
    public function getWeightFactor()
    {
        return $this->weightFactor;
    }

    /**
     * Set flower
     *
     * @param \MathBundle\Entity\Flower $flower
     *
     * @return Symptom
     */
    public function setFlower(\MathBundle\Entity\Flower $flower = null)
    {
        $this->flower = $flower;

        return $this;
    }

    /**
     * Get flower
     *
     * @return \MathBundle\Entity\Flower
     */
    public function getFlower()
    {
        return $this->flower;
    }
}
